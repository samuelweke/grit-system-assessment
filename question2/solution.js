function fibonacci(num) {
    if (num<0){
       return "Number must be not be less than 0";
    }

    if (num < 2){
      return num;
    }
    else{
      return fibonacci(num - 1) + fibonacci(num - 2);
    }
  }


  
  console.log(fibonacci(4));