

//Function to fetch data from the RandomUserApi
const fetchUsers = function() {
	const apiEndpoint = "https://randomuser.me/api/?results=100";
	fetch(apiEndpoint)
		.then(response => response.json())
		.then(data => createUI(data));
};

// Rendering the a Pie Chart using Chart.js 
const renderChart = function(data, labels) {
	const ctx = document.getElementById("genderChart").getContext("2d");
	let genderChart = new Chart(ctx, {
		type: "pie",
		data: {
			labels: labels,
			datasets: [
				{
					label: "Users ",
					data: data,
					backgroundColor: ["blue", "green"]
				}
			]
		}
	});
};


const getGender = function({ results }) {
	let maleUsers,
		femaleUsers = 0;
	maleUsers = results.reduce((total, user) => {
		if (user.gender == "male") {
			total++;
		}
		return total;
	}, 0);

	femaleUsers = results.reduce((total, user) => {
		if (user.gender == "female") {
			total++;
		}
		return total;
	}, 0);

	document.querySelector("#male").textContent = `Male: ${maleUsers}`;
	document.querySelector("#female").textContent = `Female: ${femaleUsers}`;

	data = [maleUsers, femaleUsers];
	labels = ["male", "female"];
	renderChart(data, labels);
};

const createNewElement = element => {
	return document.createElement(element);
};

const appendNewElement = (parent, element) => {
	return parent.appendChild(element);
};

const displayUserDetails = ({ results }) => {
	const usersDetails = results;
	usersDetails.map((userDetail, index) => {
		let userListItem = createNewElement("li"),
			userName = createNewElement("p"),
			userImage = createNewElement("img"),
			ul = document.querySelector("ul");

		userImage.src = userDetail.picture.medium;
		userName.innerHTML = `${userDetail.name.title.toUpperCase()} ${
			userDetail.name.first
		} ${userDetail.name.last}`;
		appendNewElement(userListItem, userImage);
		appendNewElement(userListItem, userName);
		appendNewElement(ul, userListItem);
	});
};

const createUI = data => {
	getGender(data);
	displayUserDetails(data);
};


const startApp = () => {
	fetchUsers();
};

startApp();
